﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WxReport.Code
{
    /// <summary>
    ///Msg 的摘要说明
    /// </summary>
     class Msg
    {
        private string _code;
        public string code
        {
            get { return _code; }
            set { _code = value; }
        }
        private string _msg;

        public string msg
        {

            get { return _msg; }
            set { _msg = value; }
        }
        private object _data;

        public object data
        {
            get { return _data; }
            set { _data = value; }
        }
    }

}