﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;


namespace WxReport.Code
{
    /// <summary>
    ///ProjectItem 的摘要说明
    /// </summary>
    public class ProjectItem
    {
        private string _bh;

        public string bh
        {

            get { return _bh; }
            set { _bh = value; }
        }
        private string _djrq;

        public string djrq
        {

            get { return _djrq; }
            set { _djrq = value; }
        }
        private string _tjlb;

        public string tjlb
        {
            get { return _tjlb; }
            set { _tjlb = value; }
        }
        private string _url;
        public string dowloadUrl
        {
            get { return _url; }
            set { _url = value; }

        }

    }

}
