﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;

namespace WxReport.Code
{
    /// <summary>
    /// PdfHelper 的摘要说明
    /// </summary>
    public class PdfHelper
    {

        /// <summary>
        /// 读取合并的pdf文件名称
        /// </summary>
        /// <param name="Directorypath">目录</param>
        /// <param name="outpath">导出的路径</param>
        public static void MergePDF(string Directorypath, string mainPdf, string outpath)
        {
            List<string> filelist2 = new List<string>();
            string m = new FileInfo(mainPdf).Name;
            System.IO.DirectoryInfo di2 = new System.IO.DirectoryInfo(Directorypath);
            FileInfo[] ff2 = di2.GetFiles("*.pdf").Where(it => !it.FullName.EndsWith(m)).ToArray();
            BubbleSort(ff2);
            filelist2.Add(mainPdf);
            foreach (FileInfo temp in ff2)
            {
                filelist2.Add(Directorypath + "\\" + temp.Name);
            }
            mergePDFFiles(filelist2, outpath);
            //DeleteAllPdf(Directorypath);
        }


        /// <summary>
        /// 冒泡排序
        /// </summary>
        /// <param name="arr">文件名数组</param>
        public static void BubbleSort(FileInfo[] arr)
        {
            for (int i = 0; i < arr.Length; i++)
            {
                for (int j = i; j < arr.Length; j++)
                {
                    if (arr[i].Name.CompareTo(arr[j].Name) == 1)//按创建时间（降序）
                    {
                        FileInfo temp = arr[i];
                        arr[i] = arr[j];
                        arr[j] = temp;
                    }
                }
            }
        }


        /// <summary>
        /// 合成pdf文件
        /// </summary>
        /// <param name="fileList">文件名list</param>
        /// <param name="outMergeFile">输出路径</param>
        public static void mergePDFFiles(List<string> fileList, string outMergeFile)
        {
            PdfReader reader;
            //Rectangle rec = new Rectangle(1660, 1000);
            Document document = new Document(PageSize.A4);
            PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(outMergeFile, FileMode.Create));
            document.Open();
            PdfContentByte cb = writer.DirectContent;
            PdfImportedPage newPage;
            for (int i = 0; i < fileList.Count; i++)
            {
                reader = new PdfReader(fileList[i]);
                int iPageNum = reader.NumberOfPages;
                for (int j = 1; j <= iPageNum; j++)
                {
                    document.NewPage();
                    newPage = writer.GetImportedPage(reader, j);
                    cb.AddTemplate(newPage, 0, 0);
                }
            }
            document.Close();
        }


        /// <summary>
        /// 删除一个文件里所有的文件
        /// </summary>
        /// <param name="Directorypath">文件夹路径</param>
        public static void DeleteAllPdf(string Directorypath)
        {
            System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(Directorypath);
            if (di.Exists)
            {
                FileInfo[] ff = di.GetFiles("*.pdf");
                foreach (FileInfo temp in ff)
                {
                    File.Delete(Directorypath + "\\" + temp.Name);
                }
            }
        }

        /**
         * 
         * 合并图片
         * */
        public static void mergeImage(string srcPdf, string dir, string outpdf)
        {
            System.IO.DirectoryInfo di2 = new System.IO.DirectoryInfo(dir);
            FileInfo[] ff2 = di2.GetFiles();
            ff2 = ff2.Where(it => !it.Name.ToLower().EndsWith(".pdf")).ToArray();
            BubbleSort(ff2);
            mergeImageFile(srcPdf, ff2.Select(it => it.FullName).ToList(), outpdf);
            File.Delete(srcPdf);
            foreach (string file in ff2.Select(it => it.FullName).ToList())
            {
                File.Delete(file);
            }
        }
        public static void mergeImageFile(string srcPdf, List<string> imagePaths, string outpdf)
        {


            int width = (int)(PageSize.A4.Width);
            int height = (int)(PageSize.A4.Height);
            Document doc = new Document(new iTextSharp.text.Rectangle(width, height));
            PdfReader reader = new PdfReader(new FileStream(srcPdf, FileMode.Open));
            MemoryStream ms = new MemoryStream();
            PdfWriter pdfwriter = PdfWriter.GetInstance(doc, new FileStream(outpdf, FileMode.Create));
            doc.Open();

            PdfContentByte cb = pdfwriter.DirectContent;
            int iPageNum = reader.NumberOfPages;
            for (int j = 1; j <= iPageNum; j++)
            {
                doc.NewPage();
                PdfImportedPage newPage = pdfwriter.GetImportedPage(reader, j);
                cb.AddTemplate(newPage, 0, 0);
            }
            foreach (string url in imagePaths)
            {

                byte[] b = File.ReadAllBytes(url);
                doc.NewPage();
                iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(b);
                if (image.Height > iTextSharp.text.PageSize.A4.Height - 25)
                {
                    image.ScaleToFit(iTextSharp.text.PageSize.A4.Width - 25, iTextSharp.text.PageSize.A4.Height - 25);
                }
                else if (image.Width > iTextSharp.text.PageSize.A4.Width - 25)
                {
                    image.ScaleToFit(iTextSharp.text.PageSize.A4.Width - 25, iTextSharp.text.PageSize.A4.Height - 25);
                }
                image.Alignment = iTextSharp.text.Image.ALIGN_MIDDLE;

                doc.Add(image);
            }
            //pdfwriter.Close();
            //pdfwriter.Dispose();
            doc.Close();
            doc.Dispose();

        }

        public static byte[] ZoomImage(byte[] buffer, int destHeight, int destWidth)
        {
            try
            {
                System.Drawing.Image sourImage = System.Drawing.Image.FromStream(new MemoryStream(buffer));
                int width = 0, height = 0;
                //按比例缩放             
                int sourWidth = sourImage.Width;
                int sourHeight = sourImage.Height;
                if (sourHeight > destHeight || sourWidth > destWidth)
                {
                    if ((sourWidth * destHeight) > (sourHeight * destWidth))
                    {
                        width = destWidth;
                        height = (destWidth * sourHeight) / sourWidth;
                    }
                    else
                    {
                        height = destHeight;
                        width = (sourWidth * destHeight) / sourHeight;
                    }
                }
                else
                {
                    width = sourWidth;
                    height = sourHeight;
                }

                System.Drawing.Bitmap destBitmap = new System.Drawing.Bitmap(destWidth, destHeight, PixelFormat.Format64bppPArgb);
                System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(destBitmap);
                g.Clear(System.Drawing.Color.White);
                //设置画布的描绘质量           
                g.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                g.DrawImage(sourImage, new System.Drawing.Rectangle((destWidth - width) / 2, (destHeight - height) / 2, width, height), 0, 0, sourImage.Width, sourImage.Height, System.Drawing.GraphicsUnit.Pixel);
                g.Dispose();
                //设置压缩质量       
                System.Drawing.Imaging.EncoderParameters encoderParams = new System.Drawing.Imaging.EncoderParameters();
                long[] quality = new long[1];
                quality[0] = 1200;
                System.Drawing.Imaging.EncoderParameter encoderParam = new System.Drawing.Imaging.EncoderParameter(System.Drawing.Imaging.Encoder.Quality, quality);
                encoderParams.Param[0] = encoderParam;

                MemoryStream ms = new MemoryStream();
                destBitmap.Save(ms, sourImage.RawFormat);
                //destBitmap.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                //destBitmap.Save("f:\\jj.jpg");
                //File.WriteAllBytes("f:\\jj.jpg", ms.ToArray());
                //sourImage.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                return ms.ToArray();
                //sourImage.Dispose();
                //return destBitmap;
            }
            catch
            {
                return new byte[] { };
            }
        }

        public static void DelectDir(string srcPath)
        {
            try
            {
                DirectoryInfo dir = new DirectoryInfo(srcPath);
                FileSystemInfo[] fileinfo = dir.GetFileSystemInfos();  //返回目录中所有文件和子目录
                foreach (FileSystemInfo i in fileinfo)
                {
                    if (i is DirectoryInfo)            //判断是否文件夹
                    {
                        DirectoryInfo subdir = new DirectoryInfo(i.FullName);
                        subdir.Delete(true);          //删除子目录和文件
                    }
                    else
                    {
                        //如果 使用了 streamreader 在删除前 必须先关闭流 ，否则无法删除 sr.close();
                        File.Delete(i.FullName);      //删除指定文件
                    }
                }
                Directory.Delete(srcPath);
            }
            catch (Exception e)
            {
                throw;
            }
        }

    }
}

