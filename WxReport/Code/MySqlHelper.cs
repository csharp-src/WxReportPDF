﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data;
using System.Data.SqlClient;

namespace WxReport.Code
{
    /// <summary>
    ///MySqlHelper 的摘要说明
    /// </summary>
    public class MySqlHelper
    {
        public static DataTable Fill(string sql, params SqlParameter[] ps)
        {
            DataTable table = new DataTable();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = new SqlCommand(sql, SqlHelper.GetConnection());
            if (ps != null && ps.Length > 0) adapter.SelectCommand.Parameters.AddRange(ps);
            adapter.Fill(table);
            return table;
        }
    }

}
