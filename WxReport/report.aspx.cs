﻿using Microsoft.Reporting.WebForms;
using MvcGuestBook.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WxReport.Code;


namespace WxReport
{
    public partial class report : System.Web.UI.Page
    {
        private string djlsh, str_tjbh, str_tjcs;
        private string basePath;
        private static object syncLock = new object();

        protected void Page_Load(object sender, EventArgs e)
        {


            //LogHelper.WriteLog("测试", new Exception("这是一个测试！"));

            string mobile = Request.QueryString["tel"];
            this.djlsh = Request.QueryString["bh"];

            Msg msg = SUCCESS();

            try
            {

                if (!string.IsNullOrEmpty(djlsh))
                {
                    exportSingle(djlsh);
                    return;
                }

                if (!string.IsNullOrEmpty(mobile))
                {
                    DataTable table = MySqlHelper.Fill("select  top 10 djlsh,djrq,tjlb,sumover from v_tj_tjdjb where mobile=@mobile and sumover>2 order by djrq desc ", new SqlParameter("@mobile", mobile));

                    List<ProjectItem> ls = new List<ProjectItem>();
                    foreach (DataRow row in table.Rows)
                    {
                        ProjectItem item = new ProjectItem();
                        item.bh = Convert.ToString(row["djlsh"]);
                        item.djrq = Convert.ToDateTime(row["djrq"]).ToString("yyyy-MM-dd HH:mm");
                        item.tjlb = row["tjlb"].ToString();
                        item.dowloadUrl = Request.Url.OriginalString.Replace(Request.Url.Query, "") + "?bh=" + item.bh;
                        ls.Add(item);

                    }

                    if (ls.Count == 0)
                    {
                        //msg.code = "1";
                        msg.msg = "没有找到相关报告！";
                    }
                    msg.data = ls;

                }
                else
                {

                    msg = FAIL();
                    msg.msg = "手机号码不能为空！";
                }
            }
            catch (Exception ex)
            {
                LogHelper.WriteLog("导出报错:"+djlsh, ex);
                msg = FAIL();
                msg.msg = ex.Message;

            }

            string json = JsonConvert.SerializeObject(msg);
            Response.ContentType = "application/json";
            Response.Write(json);





        }
        private static Msg SUCCESS()
        {
            Msg msg = new Msg();
            msg.code = "0";
            msg.msg = "成功";
            return msg;

        }
        private static Msg FAIL()
        {
            Msg msg = new Msg();
            msg.code = "1";
            msg.msg = "失败";
            return msg;

        }
        /// <summary>
        /// 获取系统参数值
        /// </summary>
        /// <param name="csbm">参数编码</param>
        /// <returns></returns>
        public string GetXtCsz(string csbm)
        {
            string strSql;
            strSql = "select csz from xt_cssz where csbm='" + csbm + "'";
            return MySqlHelper.Fill(strSql).Rows[0][0].ToString();
        }
        private void exportSingle(string djlsh)
        {
            //StreamWriter sw = new StreamWriter("d:\\time_" + DateTime.Now.Millisecond + ".txt");

            DateTime date1 = DateTime.Now;
            DataTable table = MySqlHelper.Fill("select * from v_tj_tjdjb where djlsh=@djlsh order by djrq desc", new SqlParameter("@djlsh", djlsh));

            if (table.Rows.Count == 0)
            {
                throw new Exception("没有找到相关报告！");
            }
            string str_tjdw = GetXtCsz("TjDwMc");
            string str_dwdh = GetXtCsz("TjDwDh");


            str_tjbh = table.Rows[0]["tjbh"].ToString().Trim();
            str_tjcs = table.Rows[0]["tjcs"].ToString().Trim();
            djlsh = table.Rows[0]["djlsh"].ToString().Trim();
            string sumover = table.Rows[0]["sumover"].ToString();
            basePath = Server.MapPath(null);
            string ttiel = "体检报告_" + djlsh;
            LocalReport report = new LocalReport();
            //RdlcPrintNew rdlcprint = new RdlcPrintNew();

            report.ReportPath = basePath + "/rdlcreport/Report_tjbg.rdlc"; ;
            report.SubreportProcessing += new SubreportProcessingEventHandler(report_SubreportProcessing);
            report.EnableExternalImages = true;
            report.DataSources.Clear();
            reportImages(report);
            DataTable dt2 = Get_v_tj_tjdjb(str_tjbh, str_tjcs);
            DataTable dt3 = Get_Exec_proc_get_tjxmmx(str_tjbh, str_tjcs);





            report.DataSources.Add(new ReportDataSource("PEISDataSet_v_tj_tjdjb", dt2));
            report.DataSources.Add(new ReportDataSource("PEISDataSet_proc_get_tjxmmx", dt3));

            ReportParameter rp1 = new ReportParameter("tjdw", str_tjdw);
            //ReportParameter rp2 = new ReportParameter("barcode", str_barpath);
            ReportParameter rp3 = new ReportParameter("tjdh", str_dwdh);
            //ReportParameter rp4 = new ReportParameter("yypic", str_picpath);
            //ReportParameter rp5 = new ReportParameter("yyewm", str_yyewm);

            ReportParameter rp6 = new ReportParameter("tjbh", dt2.Rows[0]["tjbh"].ToString());
            ReportParameter rp7 = new ReportParameter("xm", dt2.Rows[0]["xm"].ToString());
            ReportParameter rp8 = new ReportParameter("xb", dt2.Rows[0]["xb"].ToString());
            ReportParameter rp9 = new ReportParameter("nl", dt2.Rows[0]["nl"].ToString());

            //report.DataSources.Clear();
            report.SetParameters(new ReportParameter[] { rp1, rp3, rp6, rp7, rp8, rp9 });
            // string str = "第一段:" + (DateTime.Now - date1).TotalSeconds;
            //sw.WriteLine("第一段:" + (DateTime.Now-date1).TotalSeconds);


            DateTime date2 = DateTime.Now;

            //System.Web.HttpContext.Current.Response.BinaryWrite(bytes);
            Export(report, "Image", ttiel, djlsh);





        }
        private static void line2table(DataTable table, string strs)
        {

            foreach (string str in strs.Split(Environment.NewLine.ToCharArray()))
            {
                DataRow row = table.NewRow();
                row["z1"] = str;
                row["z2"] = "zs";
                row["z3"] = "False";
                table.Rows.Add(row);
            }
        }

        public DataTable Get_Exec_proc_get_tjxmmx(string tjbh, string tjcs)
        {
            string strSql;
            strSql = "exec proc_get_tjxmmx '" + tjbh + "','" + tjcs + "'";
            return MySqlHelper.Fill(strSql);
        }

        //StringBuilder sb = new StringBuilder();
        void report_SubreportProcessing(object sender, SubreportProcessingEventArgs e)
        {


            string reportName = e.ReportPath;
            //SqlHelper helper = new SqlHelper();
            //rdlcBiz rdlcbiz = new rdlcBiz();
            //Console.WriteLine(e.ReportPath);
            DataTable dt1 = Get_TJ_TJBG_BZB(str_tjbh, str_tjcs);
            DataTable dt2 = Get_v_tj_tjdjb(str_tjbh, str_tjcs);
            DataTable dtZhxmmx = new DataTable();
            dtZhxmmx = GetTjjlmx(str_tjbh, str_tjcs);
            //sb.AppendLine(reportName + ":" + "PEISDataSet_v_tjjlmx");
            //sb.AppendLine(reportName);
            if (reportName.IndexOf("report_tjzhxmmx.rdlc")>-1)
            {
                //IsCopy isCopy=
                IsCopy isCopy = row => !(row["lxbh"].ToString().Trim() == "06");
                DataTable table = CopyData(isCopy, dtZhxmmx);
                e.DataSources.Add(new ReportDataSource("PEISDataSet_v_tjjlmx", table));
                
            }
            if (reportName.IndexOf("report_tjzhxmmx_jy.rdlc")>-1)
            {
                IsCopy isCopy = row => row["lxbh"].ToString().Trim() == "06"; ;
                DataTable table = CopyData(isCopy, dtZhxmmx);
                //File.AppendAllText("e:\\ll.txt", "report_tjzhxmmx_jy.rdlc:"+table.Rows.Count);
                e.DataSources.Add(new ReportDataSource("PEISDataSet_v_tjjlmx_jy", table));
                //sb.AppendLine(reportName + ":" + "PEISDataSet_v_tjjlmx");
            }


            e.DataSources.Add(new ReportDataSource("PEISDataSet_tj_tjjlmxb", dt1));
            e.DataSources.Add(new ReportDataSource("PEISDataSet_v_tj_tjdjb", dt2));

            string zs = dt2.Rows[0]["zs"].ToString();
            string jy = dt2.Rows[0]["jy"].ToString();

            DataTable str2lineTable = new DataTable();
            str2lineTable.Columns.Add(new DataColumn("z1", typeof(string)));
            str2lineTable.Columns.Add(new DataColumn("z2", typeof(string)));
            str2lineTable.Columns.Add(new DataColumn("z3", typeof(string)));

            DataRow row1 = str2lineTable.NewRow();
            row1["z1"] = "您本次体检在选择的项目中，以下请您注意：";
            row1["z2"] = "zs";
            row1["z3"] = "True";
            str2lineTable.Rows.Add(row1);

            line2table(str2lineTable, zs);
            DataRow row2 = str2lineTable.NewRow();
            row2["z1"] = "针对以上印象，如下建议供您参考：";
            row2["z2"] = "zs";
            row2["z3"] = "True";
            str2lineTable.Rows.Add(row2);

            line2table(str2lineTable, jy);
            e.DataSources.Add(new ReportDataSource("PEISDataSet_v_zyjk_jgtzs_header", str2lineTable));
        }
        public bool IsCopy1(DataRow row)
        {
            return !(row["lxbh"].ToString().Trim() == "06");
        }
        public bool IsCopy2(DataRow row)
        {
            return !IsCopy1(row);
        }
        public delegate bool IsCopy(DataRow row);


        /// <summary>
        /// 检验科数据特别处理
        /// </summary>
        /// <param name="passed"></param>
        /// <param name="sourceTable"></param>
        /// <returns></returns>
        public static DataTable CopyData(IsCopy isCopy, DataTable sourceTable)
        {
            DataTable table1 = sourceTable.Copy();
            table1.Clear();
            foreach (DataRow row in sourceTable.Rows)
            {
                //Console.WriteLine(row["lxbh"].ToString());

                if (isCopy(row))
                {
                    DataRow newRow = table1.NewRow();

                    foreach (DataColumn col in table1.Columns)
                    {
                        newRow[col.ColumnName] = row[col.ColumnName];
                    }
                    table1.Rows.Add(newRow);
                }


            }
            return table1;

        }
        public DataTable Get_TJ_TJBG_BZB(string tjbh, string tjcs)
        {
            string strSql;
            strSql = "exec PROC_TJ_TJBG_BZBG '" + tjbh + "','" + tjcs + "'";
            return null;
        }
        public DataTable Get_v_tj_tjdjb(string tjbh, string tjcs)
        {
            string strSql;
            //strSql = "select * from v_tj_tjdjb where tjbh='" + tjbh + "' and tjcs='" + tjcs + "'";
            strSql = "select djlsh,xm,xb,nl,csrq,hyzk,mz,tjrq,djrq,tjbh,tjcs,dwmc,bmmc,tjlb,lbbh,rylb,mobile,phone,address,sfzh,sykh,dwbh,bmbh,sumover,picture,barcode,xbcode,zs,jy,jcrq,jcys,tjjl,jktj,czy,tcmc,zjys,zjys_img,fcrq,fcgy,rylbcode,rylbmc,gz,whcd,whcdbm,gzbh,lx,whysmc,whys,lb,wyyslb,gl,zytjjl,zytjjy,tjbgwxts,remark from v_tj_tjdjb where tjbh='" + tjbh + "' and tjcs='" + tjcs + "'";

            return MySqlHelper.Fill(strSql, null);

        }
        /// <summary>
        /// 获取体检记录明细
        /// </summary>
        /// <param name="tjbh"></param>
        /// <param name="tjcs"></param>
        /// <returns></returns>
        public DataTable GetTjjlmx(string tjbh, string tjcs)
        {
            string sql = "select * from v_tjjlmx where tjbh='" + tjbh + "' and tjcs='" + tjcs + "' order by lx_disp_order,disp_order,xmb_disp_order";


            // string sql = "select tjbh,tjcs,djlsh,xm,xb,tjxmbh,jg,mc,dw,zcts,ckz,ts,xj,jcrq,jcys,zhmc,jcjylx,sfyx from v_tjjlmx where tjbh='" + tjbh + "' and tjcs='" + tjcs + "' order by disp_order,xmb_disp_order";

            return MySqlHelper.Fill(sql, null);
        }
        public DataTable imgTable()
        {

            DataTable imgFile = MySqlHelper.Fill("select  * from v_files where bh=@bh order by forder,ctime", new SqlParameter("@bh", this.djlsh));
            return imgFile;
        }

        private void reportImages(LocalReport report)
        {
            //string str_barpath =Server.MapPath(null);

            string str_barpath = Server.MapPath("barcode.png");

            //BarcodeControl barcode = new BarcodeControl();
            //barcode.BarcodeType = BarcodeType.CODE128C;
            //barcode.Data = djlsh;
            //barcode.CopyRight = "";
            MemoryStream stream = new MemoryStream();
            //barcode.MakeImage(ImageFormat.Png, 1, 50, true, false, null, stream);
            BarCode128 code = new BarCode128();
            code.ValueFont = new System.Drawing.Font("Arial", 9);//声明条码下方的字体  
            Bitmap bitmap = code.GetCodeImage(djlsh, BarCode128.Encode.Code128C);
            bitmap.Save(stream, ImageFormat.Png);
            string str_barcode = Convert.ToBase64String(stream.ToArray());
            string str_picpath = basePath + ("/Img/医院徽标.bmp");

            string str_ZYBG_Top = basePath + "/Img/ZYBG_Top.png";
            string str_yyewm = basePath + "/Img/微信.png";
            string str_log = basePath + "/Img/log.jpg";

            //myimge.Save(str_barpath, ImageFormat.Png);

            str_picpath = "file:///" + str_picpath;//医院徽标
            str_barpath = "file:///" + str_barpath;//条码
            str_ZYBG_Top = "file:///" + str_ZYBG_Top;//藏医体检
            str_yyewm = "file:///" + str_yyewm;//微信二维码
            str_log = "file:///" + str_log;//LOG标志


            //ReportParameter rp1 = new ReportParameter("tjdw", "上海金科医疗");
            ReportParameter rp2 = new ReportParameter("barcode", str_barcode);
            //ReportParameter rp3 = new ReportParameter("sy_barcode", str_barcode);
            //ReportParameter rp3 = new ReportParameter("tjdh", djlsh);
            ReportParameter rp4 = new ReportParameter("yypic", str_picpath);
            ReportParameter rp5 = new ReportParameter("yyewm", str_yyewm);
            //report.DataSources.Clear();
            report.SetParameters(new ReportParameter[] { rp2, rp4, rp5 });



        }
        List<MemoryStream> files = new List<MemoryStream>();
        private Stream CreateStream(string name, string extension, Encoding encoding, string mimeType, bool willSeek)
        {
            System.IO.MemoryStream ms = new MemoryStream();
            files.Add(ms);
            return ms;
        }



        //private string tmpDir = "";
        /**
         * 下载体检人员报告文件
         * */
        public void DownloadFiles(string tmpDir)
        {
            DataTable table = imgTable();

            if (table.Rows.Count == 0) return;


            ////TODO:
            //File.Copy("f:\\1.jpg", tmpDir + "/1.jpg");
            //File.Copy("f:\\1.pdf", tmpDir + "/1.pdf");
            //return;

            System.Net.WebClient client = new System.Net.WebClient();
            for (int i = 0; i < table.Rows.Count; i++)
            {
                try
                {
                    DataRow row = table.Rows[i];
                    string order = row["forder"].ToString();
                    string ctime = DateTime.Parse(row["ctime"].ToString()).ToString("yyyyMMddHHmmss");
                    string fileName = tmpDir + "/" + (order+"_"+ctime) + row["ID"].ToString() + row["ext"].ToString();
                    client.DownloadFile(row["loc"].ToString(), fileName);
                }
                finally
                {
                    client.Dispose();
                }

            }
        }

        public bool Export(LocalReport report, string exportType, string reportsTitle, string djlsh)
        {
            string tmpDir = Server.MapPath("tmp/" + Guid.NewGuid().ToString());
            if (!Directory.Exists(tmpDir))
            {
                Directory.CreateDirectory(tmpDir);
            }

            //删除过期的临时目录
            //TODO:
            string[] dirs = Directory.GetDirectories(Server.MapPath("tmp"));
            foreach (string dir in dirs)
            {
                if ((DateTime.Now - Directory.GetLastWriteTime(dir)).TotalMinutes > 5)
                {
                    Directory.Delete(dir, true);
                    //PdfHelper.DelectDir(dir);
                }
            }
            var pdfFileName = djlsh + ".pdf";

            //生成ＰＤＦ报告在本地，以节省内存开销
            Action genpdf = () =>
            {
                Warning[] warnings = null;
                string deviceInfo =
                           "<DeviceInfo>" +
                           "  <OutputFormat>PDF</OutputFormat>" +
                           "  <PageWidth>21cm</PageWidth>" +
                           "  <PageHeight>29.7cm</PageHeight>" +
                           "  <MarginTop>1cm</MarginTop>" +
                           "  <MarginLeft>1cm</MarginLeft>" +
                    //"  <MarginRight>1cm</MarginRight>" +
                    //" <DpiX>300</DpiX>"+
                    // "<DpiY>300</DpiY>"+
                           "  <MarginBottom>1.3cm</MarginBottom>" +
                           "</DeviceInfo>";


                //report.Render(filetype, deviceInfo,c, out warnings);


                string mimeType;
                string encoding;
                string extension;
                string[] streamids;
                string basePath = Server.MapPath(null);
                var buffer = report.Render("PDF", deviceInfo, out mimeType, out encoding, out extension,
                                 out streamids, out warnings);
                
                File.WriteAllBytes(tmpDir + "/" + pdfFileName, buffer);
            };

            //下载远程图片
            Action downloadfile = () =>
            {
                DownloadFiles(tmpDir);
            };
            //并行运行，提高响应速度
            Parallel.Invoke(downloadfile, genpdf);


            //合并报告
            //1.合并图片
            string meregeWithImagePdf = tmpDir + "/meregeWithImagePdf.pdf";
            try
            {

                PdfHelper.mergeImage(tmpDir + "/" + pdfFileName, tmpDir, meregeWithImagePdf);
            }
            catch(Exception ex)
            {
                LogHelper.WriteLog("合并:" + djlsh, ex);       
            }
            
            //2.合并ＰＤＦ
            string pdfName = tmpDir + "/99999999-final.pdf";
            PdfHelper.MergePDF(tmpDir, meregeWithImagePdf, pdfName);


            System.Web.HttpContext.Current.Response.Buffer = true;
            System.Web.HttpContext.Current.Response.Clear();
            System.Web.HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.UTF8;
            System.Web.HttpContext.Current.Response.ContentType = "application/pdf";
            Response.WriteFile(pdfName);
            System.Web.HttpContext.Current.Response.Flush();
            System.Web.HttpContext.Current.Response.End();

            return true;
        }
    }
}